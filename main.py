from interface import MainWindow
from PyQt5.QtWidgets import QApplication
import sys
from argparse import ArgumentParser

if __name__ == '__main__':
    app = QApplication([])
    w = MainWindow()
    sys.exit(app.exec_())

