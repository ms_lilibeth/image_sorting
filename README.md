Hello! ^_^ 

That's my tool for sorting images on clip-art, set, background, photo and illustration.

**Hotkeys for checkboxes:**
 
 - S - set
 - D - background
 - C - clip-art
 - I - illustration
 - P - photo
 
**Other shortcuts**

- Space - accept
- Del - delete
- Ctrl+Z - undo (works for one of the last images only, doesn't work for deleted images)


**Usage**: python3 main.py

**Requires** pyqt5


> Written with [StackEdit](https://stackedit.io/).