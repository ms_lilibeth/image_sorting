from PyQt5.QtWidgets import (QPushButton, QWidget, QLabel, QVBoxLayout, QHBoxLayout,
                             QCheckBox, QFileDialog, QMainWindow)
from PyQt5.QtCore import QSize, Qt, pyqtSignal
from PyQt5.QtGui import QIcon, QPixmap, QKeyEvent, QKeySequence
from os import listdir, mkdir, remove
from os.path import isfile, join, exists, basename
from shutil import copy

labels = ['clip-art', 'set', 'background', 'photo', 'illustration']


class MainWindow(QMainWindow):
    ca_signal = pyqtSignal(name='ca_checked')
    set_signal = pyqtSignal(name='set_checked')
    bg_signal = pyqtSignal(name='bg_checked')
    ph_signal = pyqtSignal(name='ph_checked')
    ill_signal = pyqtSignal(name='ill_checked')
    delete_signal = pyqtSignal(name='delete_img')
    accept_signal = pyqtSignal(name='accept_img')
    cancel_signal = pyqtSignal(name='cancel')

    def __init__(self):
        super(MainWindow, self).__init__()

        self.ca_chb = QCheckBox('clip-art')
        self.set_chb = QCheckBox('set')
        self.bg_chb = QCheckBox('background')
        self.ph_chb = QCheckBox('photo')
        self.ill_chb = QCheckBox('illustration')
        self.accept_btn = QPushButton('Accept')
        self.delete_btn = QPushButton('Delete')

        self.input_lbl = QLabel('Input path: <no path provided>')
        self.output_lbl = QLabel('Output path: <no path provided>')
        self.get_input_btn = QPushButton()
        self.get_output_btn = QPushButton()

        self.img_label = QLabel(self)
        self.img_label_max_width = 400
        self.img_label_max_height = 400
        self.default_pixmap = QPixmap('no_img.png')
        self.current_img_fp = ''

        # connect widgets
        self.get_input_btn.clicked.connect(self.choose_input_folder)
        self.get_output_btn.clicked.connect(self.choose_output_folder)
        self.accept_btn.clicked.connect(self.on_accept)
        self.delete_btn.clicked.connect(self.on_delete)

        # connect signals
        self.ca_signal.connect(self.on_ca_signal)
        self.set_signal.connect(self.on_set_signal)
        self.bg_signal.connect(self.on_bg_signal)
        self.ph_signal.connect(self.on_ph_signal)
        self.ill_signal.connect(self.on_ill_signal)
        self.delete_signal.connect(self.on_delete)
        self.accept_signal.connect(self.on_accept)
        self.cancel_signal.connect(self.on_cancel)

        self.input_fp = ''
        self.output_fp = ''
        self.flist = []
        self.enabled = False
        self.last_action = {}  # {'src': '', 'labels': []}

        self.initUI()
        self.setWindowTitle('Image sorting')
        self.setWindowIcon(QIcon('./cherry.png'))
        self.show()

    def initUI(self):
        self.img_label.setPixmap(self.default_pixmap)
        self.get_input_btn.setIcon(QIcon("./settings.png"))
        self.get_input_btn.setIconSize(QSize(20, 20))
        self.get_input_btn.resize(QSize(20, 20))
        self.get_output_btn.setIcon(QIcon("./settings.png"))
        self.get_output_btn.setIconSize(QSize(20, 20))
        self.get_output_btn.resize(QSize(20, 20))

        self.accept_btn.setStyleSheet("background-color: green")
        self.delete_btn.setStyleSheet("background-color: red")

        # NoFocus Policy (otherwise Key_Space event is not catched properly)
        self.get_input_btn.setFocusPolicy(Qt.NoFocus)
        self.get_output_btn.setFocusPolicy(Qt.NoFocus)
        self.delete_btn.setFocusPolicy(Qt.NoFocus)
        self.ca_chb.setFocusPolicy(Qt.NoFocus)
        self.set_chb.setFocusPolicy(Qt.NoFocus)
        self.bg_chb.setFocusPolicy(Qt.NoFocus)
        self.ph_chb.setFocusPolicy(Qt.NoFocus)
        self.ill_chb.setFocusPolicy(Qt.NoFocus)

        # vbox_container -- main container
        vbox_container = QVBoxLayout()

        # hbox_input -- for input filepath label and settings button
        hbox_input = QHBoxLayout()
        hbox_input.addWidget(self.get_input_btn, alignment=Qt.AlignLeft)
        hbox_input.addWidget(self.input_lbl, alignment=Qt.AlignLeft)
        hbox_input.addStretch(1)

        # hbox_output -- for input filepath label and settings button
        hbox_output = QHBoxLayout()
        hbox_output.addWidget(self.get_output_btn, alignment=Qt.AlignLeft)
        hbox_output.addWidget(self.output_lbl, alignment=Qt.AlignLeft)
        hbox_output.addStretch(1)

        # hbox_img -- for image and checkboxes
        hbox_img = QHBoxLayout()
        hbox_img.addWidget(self.img_label)

        # vbox_checklist -- for checkboxes
        vbox_checklist = QVBoxLayout()
        vbox_checklist.addWidget(self.ca_chb)
        vbox_checklist.addWidget(self.set_chb)
        vbox_checklist.addWidget(self.bg_chb)
        vbox_checklist.addWidget(self.ph_chb)
        vbox_checklist.addWidget(self.ill_chb)
        vbox_checklist.addWidget(self.accept_btn)
        vbox_checklist.addWidget(self.delete_btn)

        hbox_img.addLayout(vbox_checklist)

        vbox_container.addLayout(hbox_input)
        vbox_container.addLayout(hbox_output)
        vbox_container.addStretch(1)
        vbox_container.addLayout(hbox_img)

        main_widget = QWidget(self)
        main_widget.setLayout(vbox_container)
        self.setCentralWidget(main_widget)
        self.statusBar().showMessage('Choose input and output folders')
        self.set_disabled()

    def keyPressEvent(self, event):
        if type(event) == QKeyEvent:
            if event.key() == Qt.Key_C:
                self.ca_signal.emit()
            elif event.key() == Qt.Key_S:
                self.set_signal.emit()
            elif event.key() == Qt.Key_D:
                self.bg_signal.emit()
            elif event.key() == Qt.Key_P:
                self.ph_signal.emit()
            elif event.key() == Qt.Key_I:
                self.ill_signal.emit()
            elif event.key() == Qt.Key_Delete:
                self.delete_signal.emit()
            elif event.key() == Qt.Key_Space:
                self.accept_signal.emit()
            elif event.matches(QKeySequence.Undo):
                self.cancel_signal.emit()

    def choose_input_folder(self):
        dialog = QFileDialog()
        folder = str(QFileDialog.getExistingDirectory(dialog))
        if folder == '':
            return
        self.input_lbl.setText('Input path: %s' % folder)
        self.input_fp = folder
        self.flist = [join(folder, f) for f in listdir(folder) if isfile(join(folder, f))]
        if self.output_fp != '':
            self.get_next_img()

    def choose_output_folder(self):
        dialog = QFileDialog()
        folder = str(QFileDialog.getExistingDirectory(dialog))
        if folder == '':
            return
        self.output_lbl.setText('Output path: %s' % folder)
        self.output_fp = folder
        for label in labels:
            if not exists(join(folder, label)):
                mkdir(join(folder, label))
        if self.input_fp != '':
            self.get_next_img()

    def get_next_img(self):
        self.clean_checkboxes()
        if len(self.flist) > 0 and self.enabled is False:
            self.set_enabled()
        if len(self.flist) == 0 and self.enabled is True:
            self.set_disabled()
            self.img_label.setPixmap(self.default_pixmap)
            self.current_img_fp = ''

        while len(self.flist) > 0:
            fp = self.flist[0]
            pixmap = QPixmap(fp)
            img_tmp = pixmap.toImage()
            if img_tmp.width() == 0 or img_tmp.height() == 0:
                del self.flist[0]
                continue
            if img_tmp.width() > self.img_label_max_width or img_tmp.height() > self.img_label_max_height:
                pixmap = pixmap.scaled(self.img_label_max_width, self.img_label_max_height, Qt.KeepAspectRatio)
            self.img_label.setPixmap(pixmap)
            self.current_img_fp = fp
            self.statusBar().showMessage(basename(fp))
            del self.flist[0]
            return
        self.img_label.setPixmap(self.default_pixmap)
        self.current_img_fp = ''

    def clean_checkboxes(self):
        self.ca_chb.setChecked(False)
        self.set_chb.setChecked(False)
        self.ill_chb.setChecked(False)
        self.ph_chb.setChecked(False)
        self.bg_chb.setChecked(False)

    def set_disabled(self):
        """
        Set checkboxes and accept/delete buttons disabled
        """
        self.accept_btn.setEnabled(False)
        self.delete_btn.setEnabled(False)
        self.ca_chb.setEnabled(False)
        self.set_chb.setEnabled(False)
        self.bg_chb.setEnabled(False)
        self.ph_chb.setEnabled(False)
        self.ill_chb.setEnabled(False)
        self.enabled = False

    def set_enabled(self):
        """
        Set checkboxes and accept/delete buttons enabled
        """
        self.accept_btn.setEnabled(True)
        self.delete_btn.setEnabled(True)
        self.ca_chb.setEnabled(True)
        self.set_chb.setEnabled(True)
        self.bg_chb.setEnabled(True)
        self.ph_chb.setEnabled(True)
        self.ill_chb.setEnabled(True)
        self.enabled = True

    def on_accept(self):
        if self.current_img_fp != '':
            self.last_action = {'src': self.current_img_fp, 'labels': []}
            if self.ca_chb.isChecked() and not (self.ph_chb.isChecked() or self.ill_chb.isChecked()):
                return
            if self.set_chb.isChecked() and not (self.ph_chb.isChecked() or self.ill_chb.isChecked()
                                                 or self.bg_chb.isChecked()):
                return
            if self.ph_chb.isChecked() and self.ill_chb.isChecked():
                return
            any_checked = False
            if self.ca_chb.isChecked():
                copy(self.current_img_fp, join(self.output_fp, 'clip-art'))
                self.last_action['labels'].append('clip-art')
                any_checked = True
            if self.set_chb.isChecked():
                copy(self.current_img_fp, join(self.output_fp, 'set'))
                self.last_action['labels'].append('set')
                any_checked = True
            if self.bg_chb.isChecked():
                copy(self.current_img_fp, join(self.output_fp, 'background'))
                self.last_action['labels'].append('background')
                any_checked = True
            if self.ph_chb.isChecked():
                copy(self.current_img_fp, join(self.output_fp, 'photo'))
                self.last_action['labels'].append('photo')
                any_checked = True
            if self.ill_chb.isChecked():
                copy(self.current_img_fp, join(self.output_fp, 'illustration'))
                self.last_action['labels'].append('illustration')
                any_checked = True
            if any_checked:
                remove(self.current_img_fp)  # TODO: try-except, FileNotFoundError
            self.get_next_img()

    def on_delete(self):
        if self.current_img_fp != '':
            remove(self.current_img_fp)  # TODO: try-except, FileNotFoundError
            self.get_next_img()

    def on_cancel(self):
        if self.last_action == {}:
            return
        for lbl in self.last_action['labels']:
            # TODO: try-except, FileNotFoundError
            copy(join(self.output_fp, lbl, basename(self.last_action['src'])), self.input_fp)
            remove(join(self.output_fp, lbl, basename(self.last_action['src'])))
        self.flist.insert(0, self.current_img_fp)
        self.flist.insert(0, self.last_action['src'])
        self.last_action = {}
        self.get_next_img()

    def on_ca_signal(self):
        if not self.ca_chb.isChecked():
            self.ca_chb.setChecked(True)
        else:
            self.ca_chb.setChecked(False)

    def on_set_signal(self):
        if not self.set_chb.isChecked():
            self.set_chb.setChecked(True)
        else:
            self.set_chb.setChecked(False)

    def on_bg_signal(self):
        if not self.bg_chb.isChecked():
            self.bg_chb.setChecked(True)
        else:
            self.bg_chb.setChecked(False)

    def on_ph_signal(self):
        if not self.ph_chb.isChecked():
            self.ph_chb.setChecked(True)
        else:
            self.ph_chb.setChecked(False)

    def on_ill_signal(self):
        if not self.ill_chb.isChecked():
            self.ill_chb.setChecked(True)
        else:
            self.ill_chb.setChecked(False)
